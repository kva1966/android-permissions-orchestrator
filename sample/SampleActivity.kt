package co.baskara.pokemylocation

import android.Manifest.permission.*
import android.app.Activity
import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.widget.Button
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import co.baskara.android.permissionsorchestrator.*


class SampleActivity : Activity(), CanRequestPermissions {
  @BindView(R.id.location_button)
  lateinit var locationButton: Button

  @BindView(R.id.camera_button)
  lateinit var cameraButton: Button

  private val permsOrchestrator: PermissionsOrchestrator
    get() = PermissionsOrchestrator(this)

  @OnClick(R.id.location_button)
  fun showLocation() {
    orchestratePermissionsRequest(REQUEST_CODE_LOCATION_PERMS)
  }

  @OnClick(R.id.camera_button)
  fun videoService() {
    orchestratePermissionsRequest(REQUEST_CODE_VIDEOTAPE_PERMS)
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.sample_activity)
    ButterKnife.bind(this)
  }

  /**
   * Orchestration starting point for each permission group request.
   */
  private fun orchestratePermissionsRequest(requestCode: PermissionRequestCode): Unit {
    val permGroup = getPermGroup(requestCode)
    val perms = permGroup.permissions

    permsOrchestrator
        .orchestrate(
            Pair(permGroup.requestCode, perms),
            this::onAllPermissionsGranted,
            this::onPermissionsRationaleRequired
        )
  }

  /**
   * Client must override the activity's onRequestPermissionsResult, this cannot be
   * avoided, since the orchestrator itself is not the requesting activity.
   *
   * However, the client can pass on the results to simplify by the orchestrator.
   */
  override fun onRequestPermissionsResult(requestCode: PermissionRequestCode, permissions: Array<out String>, grantResults: IntArray) {
    // Orchestrator delegate handler.
    permsOrchestrator
        .onRequestPermissionsResult(
            RawRequestPermissionsResult(requestCode, permissions, grantResults),
            this::handleOnRequestPermissionsResult,
            { code, perms -> Log.i(TAG, "Request Perms Cancelled $code -> $perms") }
        )
  }

  /**
   * Client callback receiving a simpler view of onRequestPermissionsResult parameters.
   */
  private fun handleOnRequestPermissionsResult(res: ProcessedRequestPermissionsResult): Unit {
    Log.i(TAG, "Handling On Request Permissions -> $res")

    val (code, _, denied, permanentlyDenied) = res
    val permGroup = getPermGroup(code)

    /*
     * Since granted perms can be a subset of all perms required (e.g. due
     * to multiple requests for it), we don't really care about each grant,
     * just that as a whole we have the perms we need.
     */
    if (permsOrchestrator.allGranted(permGroup.permissions)) {
      // if all granted, this group should not have any denied permissions
      assert(denied.isEmpty())
      assert(permanentlyDenied.isEmpty())

      onAllPermissionsGranted(code, permGroup.permissions)
      return
    }

    /*
     * Display rationale for temporarily denied ones, and ask again.
     */
    if (denied.isNotEmpty()) {
      onPermissionsRationaleRequired(code, denied)
    }

    /*
     * Permanently denied, user selected a never-ask-again type checkbox
     * when perms were prompted.
     *
     */
    if (permanentlyDenied.isNotEmpty()) {
      // ... notify user.
      // We can no longer ever programmatically request these permissions
      // But we know about them, if required for app, the user needs to
      // be informed, and asked to enable it via Settings -> Apps -> Permissions
    }
  }

  private fun gs(id: Int, vararg params: Any): String {
    return resources.getString(id, *params)
  }

  private fun onPermissionsRationaleRequired(requestCode: PermissionRequestCode, perms: List<Permission>) {
    Log.i(TAG, "Rationale Needed $requestCode -> $perms")

    val ab: AlertDialog.Builder = AlertDialog.Builder(this)

    /*
     * Show rationale, then issue request for relevant perms.
     */
    ab.setTitle(gs(R.string.permission_dialog_title))
        .setMessage(getPermGroup(requestCode).rationaleId)
        .setPositiveButton(
            R.string.permission_dialog_ok, { _, _: Int ->
          //
          // Issue using appropriate orchestrator function
          //
          permsOrchestrator.requestAfterRationaleShown(requestCode, perms)
        })
        // perhaps also .setNegativeButton() -- a cancel button that does nothing
        .create()
        .show()
  }

  private fun onAllPermissionsGranted(code: PermissionRequestCode, perms: List<Permission>): Unit {
    assert(code in PERMISSION_MAP)
    assert(setOf(perms) == setOf(getPermGroup(code).permissions.toSet()))

    /*
     * Central handling point for handling fully granted permission groups.
     */
    when (code) {
      REQUEST_CODE_LOCATION_PERMS -> {
        Log.i(TAG, "Location Permissions Granted | Requesting Service Start")
        // .. do app functionality .. e.g. log GPS
      }

      REQUEST_CODE_VIDEOTAPE_PERMS -> {
        Log.i(TAG, "Video tape Permissions Granted")
        // .. do app functionality .. e.g. record a scene
      }

      else -> throw IllegalStateException("Unknown request code[$code]")
    }
  }

  companion object {
    val TAG: String = SampleActivity::class.java.simpleName
    const val REQUEST_CODE_LOCATION_PERMS: PermissionRequestCode = 1
    const val REQUEST_CODE_VIDEOTAPE_PERMS: PermissionRequestCode = 2

    /*
     * Group permissions by functionality. Rationale is also at a group
     * rather than per-permission level.
     */
    val PERMISSION_MAP: Map<PermissionRequestCode, PermissionGroup> = hashMapOf(
        REQUEST_CODE_LOCATION_PERMS to
            PermissionGroup(
                REQUEST_CODE_LOCATION_PERMS,
                R.string.location_perms_rationale,
                listOf(ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION)),

        REQUEST_CODE_VIDEOTAPE_PERMS to
            PermissionGroup(
                REQUEST_CODE_VIDEOTAPE_PERMS,
                R.string.videotape_perms_rationale,
                listOf(RECORD_AUDIO, CAMERA))
    )

    private fun getPermGroup(code: PermissionRequestCode): PermissionGroup {
      assert(code in PERMISSION_MAP)
      return PERMISSION_MAP[code]!!
    }
  }
}
