package co.baskara.android.permissionsorchestrator

import android.app.Activity
import android.content.pm.PackageManager
import android.util.Log


/**
 * Orchestration basically just passes requests on to the underlying Android system which
 * works on a request basis, identified by a request code. If a client is issuing multiple
 * requests, different codes should be used per request to allow the client to track
 * callback responses.
 */
typealias PermissionRequestCode = Int

/**
 * Permission string, type-aliased for better readability.
 */
typealias Permission = String


/**
 * Permission status, type-aliased for better readability.
 */
typealias PermissionStatus = Int


/**
 * Resource id, type-aliased for better readability.
 */
typealias PermissionRationaleResourceId = Int


/**
 * Not directly used by handler for now, however, a good approach to grouping
 * permissions in client code: a set of permissions are requested together,
 * with a common rationale and request code.
 *
 * For example, you might have a map of code -> group for a relevant function
 * of the application.
 */
data class PermissionGroup(
    val requestCode: PermissionRequestCode,
    val rationaleId: PermissionRationaleResourceId,
    val permissions: List<Permission>
)

/**
 * To pass on to [PermissionsOrchestrator.onRequestPermissionsResult] to process and
 * simplify.
 */
class RawRequestPermissionsResult(
    val code: PermissionRequestCode,
    // arrays make it unsafe to use a data class, but keeping the raw result format
    val permissions: Array<out Permission>,
    val grantResults: IntArray
) {
  operator fun component1() = code
  operator fun component2() = permissions
  operator fun component3() = grantResults
}

private typealias HandlerResultFn = (PermissionRequestCode, List<Permission>) -> Unit

typealias PermissionsRequiringRationaleFun = HandlerResultFn
typealias PermissionsGrantedFun = HandlerResultFn
typealias PermissionsDeniedFun = HandlerResultFn
typealias PermissionsRequestCancelledFun = HandlerResultFn

/**
 * Result of [PermissionsOrchestrator.onRequestPermissionsResult].
 *
 * @param code request code.
 * @param grantedPermissions granted permissions.
 * @param deniedPermissions denied permissions for which a rationale should be shown next time round
 *          before issuing another request. By the time we get to this, [Activity.shouldShowRequestPermissionRationale]
 *          will be true for any denied permissions.
 * @param permanentlyDeniedPermissions permissions that the user denied, but also issued a 'never ask again'
 *         type request when prompted, application will not receive a prompt for this and should educate the
 *         user to enable perms explicitly from Global App Settings.
 *
 * Thus contents of [deniedPermissions] and [permanentlyDeniedPermissions] are mutually exclusive.
 */
data class ProcessedRequestPermissionsResult(
    val code: PermissionRequestCode,
    val grantedPermissions: List<Permission>,
    val deniedPermissions: List<Permission>,
    val permanentlyDeniedPermissions: List<Permission>
)

/**
 * Orchestrates permission requests.
 *
 * See <a href="http://stackoverflow.com/questions/30719047/android-m-check-runtime-permission-how-to-determine-if-the-user-checked-nev">Stack Overflow Post</a>
 */
class PermissionsOrchestrator(private val component: CanRequestPermissions) {
  /**
   * Orchestration entry point.
   *
   * @param request for permissions.
   * @param allPermsAlreadyGrantedFun called if application already has all the requested perms.
   * @param displayRationaleFun notifies the client of perms for which it should display a rationale
   *          and then re-request the perms, say via [requestAfterRationaleShown]. Use a
   *          different [PermissionRequestCode] to the original request if necessary.
   */
  fun orchestrate(request: Pair<PermissionRequestCode, List<Permission>>,
                  allPermsAlreadyGrantedFun: PermissionsGrantedFun,
                  displayRationaleFun: PermissionsRequiringRationaleFun): Unit {
    val perms: List<Permission> = request.second
    val requestCode = request.first

    assert(perms.isNotEmpty(), { "No permissions supplied to orchestrate!" })

    if (allGranted(perms)) {
      Log.i(TAG, "[requestCode=$requestCode] All permissions $perms have been granted, calling back to client.")
      allPermsAlreadyGrantedFun(requestCode, perms)

      return // nothing to do
    }

    val hasStatus = { perm: Permission, status: Int -> component.checkSelfPermission(perm) == status }

    val permsGranted = perms.filter { hasStatus(it, PackageManager.PERMISSION_GRANTED) }
    val permsDenied = perms.filter { hasStatus(it, PackageManager.PERMISSION_DENIED) }
    val permsRequiringRationale = collectPermissionsNeedingRationale(permsDenied)
    val permsToRequest = permsDenied.minus(permsRequiringRationale)

    if (permsGranted.isNotEmpty()) {
      Log.i(TAG, "[requestCode=$requestCode] Already granted $permsGranted perms, not requesting.")
    }

    if (permsToRequest.isNotEmpty()) {
      Log.i(TAG, "[requestCode=$requestCode] Requesting $permsToRequest.")
      component.requestPermissions(permsToRequest.toTypedArray(), requestCode)
    }

    if (permsRequiringRationale.isNotEmpty()) {
      Log.i(TAG, "[requestCode=$requestCode] Displaying rationale for $permsRequiringRationale.")
      displayRationaleFun(requestCode, permsRequiringRationale)
    }
  }


  /**
   * To be called for permissions that [Activity.shouldShowRequestPermissionRationale] returned
   * true for and ideally, have been shown the rationale prior to calling this. This function
   * basically directly issues a request for permissions.
   *
   * Permissions are first asserted to meet the rationale-required criteria.
   */
  fun requestAfterRationaleShown(requestCode: PermissionRequestCode, permsRequiringRationale: List<Permission>): Unit {
    Log.i(TAG, "[requestCode=$requestCode] Rationale displayed, now requesting $permsRequiringRationale.")

    assert(permsRequiringRationale.isNotEmpty(), { "No permissions supplied!" })

    assert(
        permsRequiringRationale.all { component.shouldShowRequestPermissionRationale(it) },
        { "This method should only be used for permissions requiring rationale, and after a rationale is shown" }
    )
    component.requestPermissions(permsRequiringRationale.toTypedArray(), requestCode)
  }


  /**
   * A cleaner delegate to [Activity.onRequestPermissionsResult]. The client component
   * must override [Activity.onRequestPermissionsResult], then pass on the results
   * to this function. It categorises permission request results, passing them on
   * to the supplied [handlerFun].
   *
   * If a request was cancelled, e.g. by user hitting a back button, or some other
   * means, we don't get any results, the client is notified via the supplied [cancelledFun]
   * and must once more issue either [orchestrate] or [requestAfterRationaleShown]
   * as appropriate.
   *
   * Thus only one of [handlerFun] or [cancelledFun] will be called per request.
   *
   * @param incomingResult original callback results to interpret.
   * @param handlerFun client function to react to interpretation results.
   * @param cancelledFun called if request was cancelled.
   */
  fun onRequestPermissionsResult(incomingResult: RawRequestPermissionsResult,
                                 handlerFun: (ProcessedRequestPermissionsResult) -> Unit,
                                 cancelledFun: PermissionsRequestCancelledFun): Unit {
    val (code, permissions, grantResults) = incomingResult

    if (grantResults.isEmpty()) {
      Log.i(TAG, "No grant results, looks like permissions results were cancelled.")
      cancelledFun(code, permissions.toList())
      return
    }

    // Mapping of grantResults[i] -> permissions[i]
    assert(grantResults.size == permissions.size)
    val pairs: List<Pair<PermissionStatus, Permission>> = grantResults zip permissions

    /*
     * Categorise permissions.
     */
    val granted = pairs.filter { it.first == PackageManager.PERMISSION_GRANTED }.map { it.second }
    val denied = pairs.filter { it.first == PackageManager.PERMISSION_DENIED }.map { it.second }

    val deniedRequiresRationale = collectPermissionsNeedingRationale(denied)
    // If denied, and no rationale is required -> Permanently denied. User set checkbox to never ask again.
    val permanentlyDenied = denied.minus(deniedRequiresRationale)

    /*
     * Let client decide what to do next.
     */
    handlerFun(
        ProcessedRequestPermissionsResult(
            code,
            granted,
            deniedRequiresRationale,
            permanentlyDenied
        )
    )
  }


  /**
   * @param perms to check for.
   *
   * @return utility function that returns true if all perms supplied are granted.
   */
  fun allGranted(perms: List<Permission>): Boolean {
    return Companion.allGranted(component, perms)
  }

  private fun collectPermissionsNeedingRationale(perms: Iterable<Permission>): List<Permission> =
      perms.filter { component.shouldShowRequestPermissionRationale(it) }


  companion object {
    val TAG = PermissionsOrchestrator::class.simpleName

    /**
     * @param context generic context, rather than an component.
     * @param perms to check for.
     *
     * @return utility function that returns true if all perms supplied are granted.
     */
    fun allGranted(context: HasCheckSelfPermissionFeature, perms: List<Permission>): Boolean {
      assert(perms.isNotEmpty(), { "No permissions supplied!" })

      return perms.all { context.checkSelfPermission(it) == PackageManager.PERMISSION_GRANTED }
    }

  }
}
