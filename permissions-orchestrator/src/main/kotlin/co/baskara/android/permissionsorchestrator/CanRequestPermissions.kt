package co.baskara.android.permissionsorchestrator

interface HasCheckSelfPermissionFeature {
  fun checkSelfPermission(permission: Permission): PermissionStatus
}

/**
 * This is essentially to allow the one orchestrator to work with both activities
 * and fragments without too much additional work.
 */
interface CanRequestPermissions: HasCheckSelfPermissionFeature {
  fun requestPermissions(permissions: Array<Permission>, requestCode: PermissionRequestCode): Unit
  fun shouldShowRequestPermissionRationale(permission: Permission): Boolean
  fun onRequestPermissionsResult(requestCode: PermissionRequestCode, permissions: Array<out String>, grantResults: IntArray): Unit
}
