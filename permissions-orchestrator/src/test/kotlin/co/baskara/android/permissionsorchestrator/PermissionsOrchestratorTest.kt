package co.baskara.android.permissionsorchestrator

import android.Manifest
import android.app.Activity
import android.app.Fragment
import android.content.pm.PackageManager
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito
import kotlin.test.assertEquals

open class TestActivity: Activity(), CanRequestPermissions
open class TestFragment: Fragment(), CanRequestPermissions {
  override fun checkSelfPermission(permission: Permission): PermissionStatus =
    activity.checkSelfPermission(permission)
}

class PermissionsOrchestratorTest {

  private var activity: TestActivity = Mockito.mock(TestActivity::class.java)
  private val orchestrator: PermissionsOrchestrator
    get() = PermissionsOrchestrator(activity)

  // Only to test compiler
  private val _fragmentOrchestrator: PermissionsOrchestrator
    get() = PermissionsOrchestrator(TestFragment())

  @Before
  fun setUp() {
    activity = Mockito.mock(TestActivity::class.java)
  }

  @Test
  fun orchestrateAllGranted() {
    //
    // Set-up
    //

    // called internally by allGranted() call.
    Mockito.`when`(activity.checkSelfPermission(Mockito.anyString())).thenReturn(PackageManager.PERMISSION_GRANTED)
    var allGrantedFunCalled = false
    val expectedCode = 1
    val expectedPerms = listOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_WIFI_STATE)
    val allGrantedFun = { code: PermissionRequestCode, perms: List<Permission> ->
      allGrantedFunCalled = true
      assertEquals(expectedCode, code)
      assertEquals(expectedPerms, perms)
    }
    val displayRationaleFun = { _: PermissionRequestCode, _: List<Permission> ->
      throw AssertionError("Not expecting function to be called")
    }

    //
    // Run
    //
    orchestrator.orchestrate(
        Pair(expectedCode, expectedPerms),
        allGrantedFun,
        displayRationaleFun
    )

    //
    // verify
    //
    assertEquals(true, allGrantedFunCalled)
    Mockito.verify(activity, Mockito.never()).requestPermissions(ArgumentMatchers.any(Array<String>::class.java), Mockito.anyInt())
  }

  @Test
  fun orchestrateMultiScenario() {
    //
    // Set-up
    //
    val permsGranted = listOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_WIFI_STATE)
    val permsDenied = listOf(Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CALENDAR)
    // all perms requiring rationale are also denied, thus a subset.
    val permsRequiringRationale = listOf(Manifest.permission.WRITE_CALENDAR)
    val permsActuallyRequested = permsDenied.minus(permsRequiringRationale)
    val allPerms = permsGranted.plus(permsDenied)
    permsGranted.forEach {
      Mockito.`when`(activity.checkSelfPermission(it)).thenReturn(PackageManager.PERMISSION_GRANTED)
    }
    permsDenied.forEach {
      Mockito.`when`(activity.checkSelfPermission(it)).thenReturn(PackageManager.PERMISSION_DENIED)
      // default 'should show' to false first
      Mockito.`when`(activity.shouldShowRequestPermissionRationale(it)).thenReturn(false)
    }
    permsRequiringRationale.forEach {
      // override 'should show' to true for this subset.
      Mockito.`when`(activity.shouldShowRequestPermissionRationale(it)).thenReturn(true)
    }
    val expectedCode = 1
    val allGrantedFun = { _: PermissionRequestCode, _: List<Permission> ->
      throw AssertionError("Not expecting function to be called")
    }
    var displayRationaleFunCalled = false
    val displayRationaleFun = { code: PermissionRequestCode, perms: List<Permission> ->
      displayRationaleFunCalled = true
      assertEquals(expectedCode, code)
      assertEquals(permsRequiringRationale, perms)
    }

    //
    // Run
    //
    orchestrator.orchestrate(
        Pair(expectedCode, allPerms),
        allGrantedFun,
        displayRationaleFun
    )

    //
    // verify
    //
    assertEquals(true, displayRationaleFunCalled)
    Mockito.verify(activity, Mockito.times(1)).requestPermissions(permsActuallyRequested.toTypedArray(), expectedCode)
  }

  @Test
  fun requestAfterRationaleShownNoPermsSupplied() {
    assertError("No permissions supplied!") {
      orchestrator.requestAfterRationaleShown(1, listOf())
    }
  }

  @Test
  fun requestAfterRationaleShownSomePermsDoNotRequireRationale() {
    // Set-up
    val permMap = mapOf(
        Manifest.permission.ACCESS_COARSE_LOCATION to true,
        Manifest.permission.ACCESS_FINE_LOCATION to false,
        Manifest.permission.ACCESS_COARSE_LOCATION to true
    )
    permMap.forEach {
      Mockito.`when`(activity.shouldShowRequestPermissionRationale(it.key)).thenReturn(it.value)
    }

    // Run and verify
    assertError("This method should only be used for permissions requiring rationale, and after a rationale is shown") {
      orchestrator.requestAfterRationaleShown(1, permMap.keys.toList())
    }
  }

  @Test
  fun requestAfterRationaleShownOkay() {
    // Set-up
    val requestCode = 1
    val permMap = mapOf(
        Manifest.permission.ACCESS_COARSE_LOCATION to true,
        Manifest.permission.ACCESS_FINE_LOCATION to true,
        Manifest.permission.ACCESS_COARSE_LOCATION to true
    )
    permMap.forEach {
      Mockito.`when`(activity.shouldShowRequestPermissionRationale(it.key)).thenReturn(it.value)
    }
    val perms = permMap.keys.toList()

    // Run
    orchestrator.requestAfterRationaleShown(requestCode, perms)

    // verify
    Mockito.verify(activity).requestPermissions(perms.toTypedArray(), requestCode)
  }

  @Test
  fun onRequestPermissionsResultRequestCancelled() {
    //
    // Set-up
    //
    val expectedPerms = listOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_NETWORK_STATE)
    val expectedCode = 1
    val handlerFun = { _: ProcessedRequestPermissionsResult ->
      throw AssertionError("Not expecting function to be called")
    }
    var cancelledFunCalled = false
    val cancelledFun = { code: PermissionRequestCode, perms: List<Permission> ->
      cancelledFunCalled = true
      assertEquals(expectedCode, code)
      assertEquals(expectedPerms, perms)
    }
    val rawResult = RawRequestPermissionsResult(expectedCode, expectedPerms.toTypedArray(), intArrayOf())

    //
    // Run
    //
    orchestrator.onRequestPermissionsResult(rawResult, handlerFun, cancelledFun)

    //
    // Verify
    //
    assertEquals(true, cancelledFunCalled)
  }

  @Test
  fun onRequestPermissionsResultMultiScenario() {
    //
    // Set-up
    //
    val expectedCode = 1
    val permMap = mapOf(
        //
        // permission -> <status, should-show-rationale>
        //

        // granted
        Manifest.permission.ACCESS_NETWORK_STATE to Pair(PackageManager.PERMISSION_GRANTED, false),
        Manifest.permission.ACCESS_WIFI_STATE to Pair(PackageManager.PERMISSION_GRANTED, false),

        // denied
        Manifest.permission.ACCESS_COARSE_LOCATION to Pair(PackageManager.PERMISSION_DENIED, true),
        Manifest.permission.READ_CONTACTS to Pair(PackageManager.PERMISSION_DENIED, true),

        // permanently denied
        Manifest.permission.WRITE_CONTACTS to Pair(PackageManager.PERMISSION_DENIED, false),
        Manifest.permission.RECORD_AUDIO to Pair(PackageManager.PERMISSION_DENIED, false)
    )
    val perms: List<Pair<Permission, Int>> = permMap.map { (perm, pair) ->
      val (status, showRationale) = pair
      Mockito.`when`(activity.shouldShowRequestPermissionRationale(perm)).thenReturn(showRationale)
      Pair(perm, status)
    }
    val incomingResult = RawRequestPermissionsResult(
        expectedCode,
        perms.map { it.first }.toTypedArray(),
        perms.map { it.second }.toIntArray()
    )
    val cancelledFun = { _: PermissionRequestCode, _: List<Permission> ->
      throw AssertionError("Not expecting function to be called")
    }
    var handlerFunCalled = false
    val handlerFun = { res: ProcessedRequestPermissionsResult ->
      val (code, granted, denied, permanentlyDenied) = res
      handlerFunCalled = true
      assertEquals(expectedCode, code)
      assertEquals(setOf(Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.ACCESS_WIFI_STATE), granted.toSet())
      assertEquals(setOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_CONTACTS), denied.toSet())
      assertEquals(setOf(Manifest.permission.WRITE_CONTACTS, Manifest.permission.RECORD_AUDIO), permanentlyDenied.toSet())
    }

    //
    // Run
    //
    orchestrator.onRequestPermissionsResult(
        incomingResult,
        handlerFun,
        cancelledFun
    )

    //
    // Verify
    //
    assertEquals(true, handlerFunCalled)
  }

  @Test
  fun allGranted() {
    Mockito.`when`(activity.checkSelfPermission(Mockito.anyString())).thenReturn(PackageManager.PERMISSION_GRANTED)

    assertEquals(true, orchestrator.allGranted(listOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_WIFI_STATE)))
  }

  @Test
  fun allGrantedAllDenied() {
    Mockito.`when`(activity.checkSelfPermission(Mockito.anyString())).thenReturn(PackageManager.PERMISSION_DENIED)

    assertEquals(false, orchestrator.allGranted(listOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_WIFI_STATE)))
  }

  @Test
  fun allGrantedSomeDenied() {
    Mockito.`when`(activity.checkSelfPermission(Manifest.permission.ACCESS_WIFI_STATE)).thenReturn(PackageManager.PERMISSION_DENIED)
    Mockito.`when`(activity.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)).thenReturn(PackageManager.PERMISSION_GRANTED)

    assertEquals(false, orchestrator.allGranted(listOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_WIFI_STATE)))
  }

  @Test
  fun allGrantedNoPermsSupplied() {
    assertError("No permissions supplied!") {
      orchestrator.allGranted(listOf())
    }
  }

  private fun assertError(msg: String, fn: () -> Unit) {
    try {
      fn()
    } catch (e: AssertionError) {
      assertEquals(msg, e.message)
    } catch (e: Throwable) {
      throw e
    }
  }
}