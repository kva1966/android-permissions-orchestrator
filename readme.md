# Android Permissions Orchestrator

## Overview

A utility library to simplify -- well, sorta -- requesting permissions
on demand (in API Level 23 onwards).

Currently, there is a single class `PermissionsOrchestrator` in the module `permissions-orchestrator`
and its corresponding unit test. Reasonably well-documented, hopefully.

The project builds as an `AAR` file. Haven't figured out how to get it to create
source archives.


## Sample

Besides the unit test, see also `/sample/SampleActivity.kt` for an example of how 
an activity might use `PermissionsOrchestrator`.  


## Goals

1. Reduce Boilerplate: Mostly achieved.
2. More intuitive handling of permissions: Meh, leaky abstraction. It just depends
    on whether you want a gutter flood using the core API directly, or little, piddly
    drops of leakage using this utility.

I'll try to improve it as I dogfood it some more with my own applications.

Perhaps, calling it an 'orchestrator' is a tad rosy.


## License

Licensed under the terms of [Apache Software License v2.0](http://www.apache.org/licenses/LICENSE-2.0.html).


